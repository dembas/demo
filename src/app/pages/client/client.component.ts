import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/client';
import { ClientService } from 'src/app/services/client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  public clients: Client[] = [];

  public form: Client = new Client();

  constructor(private clientservice: ClientService) { }

  ngOnInit(): void {
    this.getClients();
  }

  getClients(){
    this.clientservice.list().subscribe(
      data => {
        this.clients = data as Client[];
        console.log('liste des clients', this.clients);
      },
      error => console.log(error)
    );
  }

  onSubmit(){
    if (this.form.id >= 1) {
      this.update(this.form.id, this.form);
    } else {
      this.create(this.form); 
    }
  }

  create(client: Client){  
    this.clientservice.create(client).subscribe(
      data => {
        let client: Client = data as Client;
        this.clients.push(client);
        console.log('nouveau client', data);
        this.clear();
      },
      error => console.log(error)
    );
  }

  update(id: number, client: Client){    
    this.clientservice.update(id, client).subscribe(
      data => {
        let client: Client = data as Client;
        const index = this.clients.findIndex(c => c.id === client.id);
        this.clients[index] = client;
        console.log('client modifié', data);
        this.clear();
      },
      error => console.log(error)
    );
  }

  clear(){
    this.form = new Client(); 
  }

  edit(client: Client){
    this.form.copy(client);
  }

  remove(client: Client){
    this.clientservice.delete(client.id).subscribe(
      () => {
        this.clients = this.clients.filter(c => c.id != client.id);
        console.log('client supprimé', client.id);
      }
    );
  }

}

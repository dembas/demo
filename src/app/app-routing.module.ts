import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './pages/client/client.component';
import { CommandeComponent } from './pages/commande/commande.component';

const routes: Routes = [
    {path: '**', redirectTo: 'client'},
    {path: 'client', component: ClientComponent},
    {path: 'client/:id/commandes', component: CommandeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export class Client {
    public id: number;
    public code: string; 
    public name: string; 
    public gender: 'homme' | 'femme'; //là cest deux valeurs seulemnts ()
    public phone: number; 

    constructor(){
        this.id = 0;
        this.code = '';
        this.name = '';
        this.gender = 'homme';
        this.phone = 0;
    }

    copy(client: Client){
        this.id = client.id;
        this.code = client.code;
        this.name = client.name;
        this.gender = client.gender;
        this.phone = client.phone;
    }
}
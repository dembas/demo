import { Client } from "./client";

export class Produit {
    public id: number;
    public code: string; 
    public designation: string; 
    public pu: number; 

    constructor(){
        this.id = 0;
        this.code = '';
        this.designation = '';
        this.pu = 0;
    }

    copy(produit: Produit){
        this.id = produit.id;
        this.code = produit.code;
        this.designation = produit.designation;
        this.pu = produit.pu;
    }
}
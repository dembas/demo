
export class Commande {
    public id: number;
    public code: string;
    public date: Date; 
    public montant: number; 
    public payee: boolean;
    public client_id: number;

    constructor(){
        this.id = 0;
        this.code = '';
        this.date = new Date();
        this.montant = 0;
        this.payee = false;
        this.client_id = 0;
    }

    copy(commande: Commande){
        this.id = commande.id;
        this.code = commande.code;
        this.date = commande.date;
        this.montant = commande.montant;
        this.payee = commande.payee;
        this.client_id = commande.client_id;
    }
}
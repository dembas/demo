import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { Produit } from '../models/produit';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  public path: string = 'api/produits';
  public url:string = `${environment.baseUrl}/${this.path}`;
  
  constructor(private http: HttpClient) { }

  list(){
    return this.http.get(this.url);
  }
  
  create(data: Produit){
    return this.http.post(this.url, data);
  }

  update(id: number, data: Produit){
    return this.http.put(`${this.url}/${id}`, data);
  }
  
  delete(id: number){
    return this.http.delete(`${this.url}/${id}`);
  }

}

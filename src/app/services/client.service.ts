import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { Client } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  public path: string = 'api/clients';
  public url:string = `${environment.baseUrl}/${this.path}`;

  constructor(private http: HttpClient) { }

  list(){
    return this.http.get(this.url);
  }
  
  create(data: Client){
    return this.http.post(this.url, data);
  }

  update(id: number, data: Client){
    return this.http.put(`${this.url}/${id}`, data);
  }
  
  delete(id: number){
    return this.http.delete(`${this.url}/${id}`);
  }
  
  commandes(id: number){
    return this.http.get(`${this.url}/${id}/commandes`);
  }

}

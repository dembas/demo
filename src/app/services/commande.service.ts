import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { Commande } from '../models/commande';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  public path: string = 'api/commandes';
  public url:string = `${environment.baseUrl}/${this.path}`;

  constructor(private http: HttpClient) { }

  list(){
    return this.http.get(this.url);
  }
  
  create(data: Commande){
    return this.http.post(this.url, data);
  }

  update(id: number, data: Commande){
    return this.http.put(`${this.url}/${id}`, data);
  }
  
  delete(id: number){
    return this.http.delete(`${this.url}/${id}`);
  }

}
